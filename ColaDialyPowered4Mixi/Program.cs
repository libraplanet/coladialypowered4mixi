﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using MixiAppload;

namespace ColaDialyPowered4Mixi
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * args[0] mail
             * args[1] passwd
             * args[2] dir
             */
            string mail = null;
            string pass = null;
            string dir = null;
            int sleep_sec = 10;
            //arg check
            {
                //mail
                {
                    try
                    {
                        mail = args[0];
                    }
                    catch
                    {
                    }
                }
                //pass
                {
                    try
                    {
                        pass = args[1];
                    }
                    catch
                    {
                    }
                }
                //dir
                {
                    try
                    {
                        dir = new Uri(Path.GetFullPath(args[2])).LocalPath;
                    }
                    catch
                    {
                    }
                }


                //sleep timer
                {
                    try
                    {
                        sleep_sec = int.Parse(args[3]);
                    }
                    catch
                    {
                    }
                    sleep_sec = Math.Max(10, sleep_sec);
                }
            }
            //run
            {
                if ((mail == null)
                || (pass == null)
                || (dir == null)
                || (!Directory.Exists(dir)))
                {
                    if (mail == null)
                    {
                        Console.WriteLine("mail is invalidate at args[0].");
                    }
                    if (pass == null)
                    {
                        Console.WriteLine("pass is invalidate at args[1].");
                    }
                    if (dir == null)
                    {
                        Console.WriteLine("dir is invalidate at args[2].");
                    }
                    else if (!Directory.Exists(dir))
                    {
                        Console.WriteLine("dir is notfound path at args[2].");
                        Console.WriteLine("  " + dir);
                    }
                }
                else
                {
                    //arg trace
                    {
                        {
                            Console.WriteLine("----------------------------------------------------");
                            Console.WriteLine("             Cola Diary Powered for Mixi             ");
                            Console.WriteLine("                            auther takumi");
                            Console.WriteLine("                            version 0.1");
                            Console.WriteLine("----------------------------------------------------");
                            Console.WriteLine("");
                        }
                        {
                            Console.WriteLine("arguments" );
                            Console.WriteLine("    mail = " + mail);
                            Console.WriteLine("    pass = ******(hdie)");
                            Console.WriteLine("    dir = " + dir);
                            Console.WriteLine("    sleep = " + sleep_sec + "sec");
                        }
                    }
                    //message serch
                    if(true)
                    {
                        Hashtable hashtable = new Hashtable();
                        try
                        {
                            using (FileStream stream = new FileStream(new Uri(Path.Combine(dir, "message.txt")).LocalPath, FileMode.Open, FileAccess.Read))
                            {
                                using (StreamReader reader = new StreamReader(stream))
                                {
                                    string line;
                                    string current = null;
                                    while ((line = reader.ReadLine()) != null)
                                    {
                                        //entry section
                                        {
                                            string str = line.Trim();
                                            if (str.StartsWith("[[[") & str.EndsWith("]]]"))
                                            {
                                                current = str.Substring(3, str.Length - (3 + 3));
                                                continue;
                                            }
                                        }
                                        //data section
                                        if (current != null)
                                        {
                                            if (hashtable.Contains(current))
                                            {
                                                hashtable[current] += "\r\n";
                                            }
                                            hashtable[current] += line;
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }

                        //file sccess
                        Console.WriteLine("start connection");
                        {
                            int max;
                            int uploaded_cnt = 0;
                            string msg = null;
                            List<string> files = new List<string>(Directory.GetFiles(dir, "*.jpg"));

                            //file
                            {
                                files.Sort();
                                max = files.Count;
                            }

                            Console.WriteLine("get file list. " + max + " files.");

                            //mainproc
                            MixiAppload.Manager man = new Manager(mail, pass);
                            try
                            {
                                Console.WriteLine("created manager.");
                                man.Init();
                                Console.WriteLine("inited manager.");
                                {
                                    List<string>[] errors = new List<string>[5];

                                    //errors inint
                                    {
                                        for (int i = 0; i < errors.Length; i++)
                                        {
                                            errors[i] = new List<string>();
                                        }
                                    }

                                    Console.WriteLine("start upload loop.");
                                    foreach (string fpath in files)
                                    {
                                        string key = Path.GetFileName(fpath);
                                        try
                                        {
                                            string title, discript;
                                            byte[] data;


                                            //title
                                            {
#if DEBUG
                                                title = @"****";
#else
                                                title = @"今日のコーラ";
#endif
                                            }

                                            //discript
                                            {
                                                DateTime time;
                                                //日付取得
                                                {
                                                    //タイムスタンプ取得
                                                    {
                                                        time = File.GetLastWriteTime(fpath);
                                                    }
                                                    //Exifから取得
                                                    {
                                                        using (Bitmap bitmap = new Bitmap(fpath))
                                                        {
                                                            int[] prop = bitmap.PropertyIdList;
                                                            int index = Array.IndexOf(prop, 0x9003);
                                                            try
                                                            {
                                                                PropertyItem item = bitmap.PropertyItems[index];
                                                                string s = Encoding.ASCII.GetString(item.Value);
                                                                int year = int.Parse(s.Substring(0, 4));
                                                                int month = int.Parse(s.Substring(5, 2));
                                                                int day = int.Parse(s.Substring(8, 2));
                                                                int hour = int.Parse(s.Substring(11, 2));
                                                                int min = int.Parse(s.Substring(14, 2));
                                                                int sec = int.Parse(s.Substring(17, 2));
                                                                time = new DateTime(year, month, day, hour, min, sec);
                                                            }
                                                            catch
                                                            {
                                                            }
                                                        }
                                                    }
                                                }

                                                //本文作成
                                                {
                                                    //base
                                                    {
                                                        discript = "";
                                                        discript += time.ToString("yy/MM/dd(ddd) ");
                                                        switch (time.ToString("tt"))
                                                        {
                                                            case "午前":
                                                            case "AM":
                                                                {
                                                                    discript += "AM";
                                                                    break;
                                                                }
                                                            case "午後":
                                                            case "PM":
                                                                {
                                                                    discript += "PM";
                                                                    break;
                                                                }
                                                        }
                                                        discript += time.ToString("HH:mm:ss");
                                                        discript += "のコーラ\r\n";
                                                    }
                                                    //message
                                                    {
                                                        if (hashtable.Contains(key))
                                                        {
                                                            discript += hashtable[key];
                                                            discript += "\r\n";
                                                        }
                                                    }
                                                    //footter
                                                    {
                                                        discript += "\r\n";
                                                        discript += "\r\n";
                                                        discript += "\r\n";
                                                        discript += "\r\n";
                                                        discript += @"upload from ColaDiaryPowered4Mixi.";
                                                    }
                                                }
                                            }

                                            //image binary
                                            {
                                                using (FileStream fStream = new FileStream(fpath, FileMode.Open, FileAccess.Read))
                                                {
                                                    data = new byte[fStream.Length];
                                                    fStream.Read(data, 0, data.Length);
                                                }
                                            }


                                            //add diary
                                            {
                                                man.AddDialySet(title, discript, new byte[][] { data });
                                                //Console.WriteLine("[" + key + "] " + title);
                                                //Console.WriteLine("  " + discript);
                                            }
                                            msg = "secceed.";
                                        }
                                        catch (Manager.LoginFaildException)
                                        {
                                            errors[1].Add(key);
                                            msg = "faild(LoginFaildException)";
                                        }
                                        catch (Manager.UidNotFoundException)
                                        {
                                            errors[2].Add(key);
                                            msg = "faild(UidNotFoundException)";
                                        }
                                        catch (Manager.MainDialyUploadFaildException)
                                        {
                                            errors[3].Add(key);
                                            msg = "faild(MainDialyUploadFaildException)";
                                        }
                                        catch (Manager.ConfirmDialyUploadFaildException)
                                        {
                                            errors[4].Add(key);
                                            msg = "faild(ConfirmDialyUploadFaildException)";
                                        }
                                        catch (Exception)
                                        {
                                            errors[0].Add(key);
                                            msg = "faild(Exception)";
                                        }

                                        //final
                                        {
                                            Console.WriteLine("    [" + (uploaded_cnt + 1) + "/" + max + "] " + key + " " + msg);
                                            uploaded_cnt++;
                                            if (uploaded_cnt < max)
                                            {
                                                System.Threading.Thread.Sleep(sleep_sec * 1000);
                                            }
                                        }
                                    }

                                    //errors
                                    {
                                        Console.WriteLine("errors");
                                        for (int i = 0; i < errors.Length; i++)
                                        {
                                            int j = (i + 1) % errors.Length;
                                            //
                                            {
                                                string s = "";
                                                if (j == 0)
                                                {
                                                    s = "Exception (" + errors[j].Count + "/" + max + ")";
                                                }
                                                else if (j == 1)
                                                {
                                                    s = "LoginFaildException (" + errors[j].Count + "/" + max + ")";
                                                }
                                                else if (j == 2)
                                                {
                                                    s = "UidNotFoundException (" + errors[j].Count + "/" + max + ")";
                                                }
                                                else if (j == 3)
                                                {
                                                    s = "MainDialyUploadFaildException (" + errors[j].Count + "/" + max + ")";
                                                }
                                                else if (j == 4)
                                                {
                                                    s = "ConfirmDialyUploadFaildException (" + errors[j].Count + "/" + max + ")";
                                                }
                                                Console.WriteLine("    " + s);
                                            }

                                            foreach (string s in errors[j])
                                            {
                                                Console.WriteLine("        " + s);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Manager.LoginFaildException)
                            {
                                Console.WriteLine("login faild...");
                            }
                            catch (Manager.UidNotFoundException)
                            {
                                Console.WriteLine("uid not foung...");
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("another exception...");
                            }

                            Console.WriteLine("");
                            Console.WriteLine(uploaded_cnt + "/" + max + " uploaded.");
                        }
                    }
                }
            }
        }
    }
}
