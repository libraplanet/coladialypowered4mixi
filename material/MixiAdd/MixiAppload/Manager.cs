﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.IO;
using System.Diagnostics;

namespace MixiAppload
{
    public class Manager
    {
        #region[static field]
        static string BOUNDARY = "boundryboundryboundry";// System.Environment.TickCount.ToString();
        static readonly string SITE = @"http://mixi.jp/";
        static readonly Encoding ENCODE = Encoding.GetEncoding("EUC-JP");
        #endregion

        #region[member]
        /// <summary>
        /// ユーザーIDナンバー
        /// </summary>
        int uid;

        /// <summary>
        /// ログイン用のメールアドレスとパスワード
        /// </summary>
        string mail, passwd;

        /// <summary>
        /// Cookie管理クラス
        /// </summary>
        CookieContainer cookie = new CookieContainer();
        #endregion

        #region[classes]

        #region[exceptions]
        /// <summary>
        /// ログイン失敗時の例外
        /// </summary>
        public class LoginFaildException : Exception { }

        /// <summary>
        /// UID取得失敗の例外
        /// </summary>
        public class UidNotFoundException : Exception { }

        /// <summary>
        /// メインの日記の投稿失敗の例外
        /// </summary>
        public class MainDialyUploadFaildException : Exception { }

        /// <summary>
        /// 確認の日記の投稿の失敗の例外
        /// </summary>
        public class ConfirmDialyUploadFaildException : Exception { }
        #endregion

        /// <summary>
        /// POST用データ作成クラス
        /// </summary>
        class MultiPartPostBuildWriter : StreamWriter
        {
            /// <summary>
            /// boundary
            /// </summary>
            string boundary;

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="stream"></param>
            /// <param name="boundary"></param>
            /// <param name="enc"></param>
            public MultiPartPostBuildWriter(Stream stream, string boundary, Encoding enc) : base(stream, ENCODE)
            {
                this.boundary = boundary;
            }

            /// <summary>
            /// 各始端用のboundary
            /// </summary>
            string START_BOUNDARY
            {
                get
                {
                    return "--" + boundary;
                }
            }

            /// <summary>
            /// 終端用のboundary
            /// </summary>
            string END_BOUNDARY
            {
                get
                {
                    return "--" + boundary + "--";
                }
            }

            /// <summary>
            /// 文字列データの追加
            /// </summary>
            /// <param name="key"></param>
            /// <param name="val"></param>
            public void AddFormText(string key, string val)
            {
                Flush();
                WriteLine(START_BOUNDARY);
                WriteLine("Content-Disposition: form-data; name=\"" + key + "\"");
                WriteLine();
                WriteLine(val);
                Flush();
            }


            /// <summary>
            /// JPEGデータの追加
            /// </summary>
            /// <param name="key"></param>
            /// <param name="fname"></param>
            /// <param name="data"></param>
            public void AddFormJpeg(string key, string fname, byte[] data)
            {
                Flush();
                WriteLine("--" + boundary);
                WriteLine("Content-Disposition: form-data; name=\"" + key + "\"; filename=\"photo" + fname + "\"");
                WriteLine("Content-Type: image/jpeg");
                WriteLine("Content-Transfer-Encoding: binary");
                WriteLine();
                Flush();
                BaseStream.Write(data, 0, data.Length);
                BaseStream.Flush();
                WriteLine();
                Flush();
            }

            /// <summary>
            /// 終端用のboundaryの追加
            /// </summary>
            public void AddEndBoundary()
            {
                Flush();
                WriteLine(END_BOUNDARY);
                Flush();
            }
        }

        #endregion

        #region [static quick method]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="passwd"></param>
        /// <param name="title"></param>
        /// <param name="discript"></param>
        /// <param name="imgs"></param>
        public static void QuickUpload(string mail, string passwd, string title, string discript, Image[] imgs)
        {
            byte[][] d;

            if (imgs == null)
            {
                d = null;
            }
            else
            {
                d = new byte[imgs.Length][];
                for (int i = 0; i < imgs.Length; i++)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        imgs[i].Save(mStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        d[i] = mStream.ToArray();
                    }
                }
            }
            QuickUpload(mail, passwd, title, discript, d);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="passwd"></param>
        /// <param name="title"></param>
        /// <param name="discript"></param>
        /// <param name="imgs"></param>
        public static void QuickUpload(string mail, string passwd, string title, string discript, byte[][] imgs)
        {
            Manager man = new Manager(mail, passwd);
            Console.WriteLine("[start]");
            Console.WriteLine("  [login]");
            man.Login();

            Console.WriteLine("  [get uid]");
            man.UpdateUid();
            Console.WriteLine("    uid(" + man.uid + ")");

            Console.WriteLine("  [add diary]");
            man.AddDialySet(title, discript, imgs);
            Console.WriteLine("[end...]");
        }
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="passwd"></param>
        public Manager(string mail, string passwd)
        {
            this.mail = mail;
            this.passwd = passwd;
            this.uid = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        string GetHtml(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SITE + url);
            req.CookieContainer = cookie;

            //respons
            {
                WebResponse res = req.GetResponse();
                using (Stream stream = res.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, ENCODE))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <param name="enc"></param>
        /// <returns></returns>
        static string GetResponse(HttpWebRequest req, Encoding enc)
        {
            WebResponse res = req.GetResponse();
            using (Stream stream = res.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream, enc))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fname"></param>
        /// <param name="txt"></param>
        /// <param name="enc"></param>
        /// <returns></returns>
        static void BinDump(string fname, byte[] d)
        {
            try
            {
                using (FileStream f = new FileStream(fname, FileMode.Create))
                {
                    f.Write(d, 0, d.Length);
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fname"></param>
        /// <param name="txt"></param>
        /// <param name="enc"></param>
        /// <returns></returns>
        static void TxtDump(string fname, string txt, Encoding enc)
        {
            try
            {
                using (FileStream f = new FileStream(fname, FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(f, enc))
                    {
                        w.Write(txt);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="discript"></param>
        /// <param name="datas"></param>
        public void AddDialySet(string title, string discript, byte[][] datas)
        {
            string add_res = AddDialyMain(title, discript, datas);

            //create data
            {
                string[,] keyset;

                //create
                {
                    string[] lines;
                    //lines select
                    {
                        List<string> list = new List<string>();
                        using (StringReader sReader = new StringReader(add_res))
                        {
                            string line;
                            int seq = 0;
                            while ((line = sReader.ReadLine()) != null)
                            {
                                if (seq == 0)
                                {
                                    if ("<li><form action=\"add_diary.pl\" method=\"post\">".Equals(line))
                                    {
                                        seq++;
                                    }
                                }
                                else
                                {
                                    if ("</form></li>".Equals(line))
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        list.Add(line);
                                    }
                                }
                            }
                        }
                        lines = list.ToArray();
                    }

                    //exp
                    {
                        List<string[]> list = new List<string[]>();
                        for (int i = 0; i < lines.Length; i++)
                        {
                            string key;
                            string val;
                            string line = lines[i];
                            {
                                Regex regex = new Regex("<input[\\s\\S]+name=\"(?<name>\\S+)\"");
                                Match match = regex.Match(line);
                                key = match.Groups["name"].Value;
                            }
                            if ("diary_title".Equals(key))
                            {
                                val = title;
                            }
                            else if ("diary_body".Equals(key))
                            {
                                val = discript;
                            }
                            else
                            {
                                Regex regex = new Regex("<input[\\s\\S]+value=\"(?<value>\\S*)\"");
                                Match match = regex.Match(line);
                                val = match.Groups["value"].Value;
                            }
                            list.Add(new string[] { key, val });
                        }

                        {
                            keyset = new string[list.Count, 2];
                            for (int i = 0; i < keyset.GetLength(0); i++)
                            {
                                keyset[i, 0] = list[i][0];
                                keyset[i, 1] = list[i][1];
                            }
                        }
                    }
                }

                //connection
                {
                    HttpWebRequest req;
                    {
                        byte[] d;

                        //create data
                        {
                            using (MemoryStream stream = new MemoryStream())
                            {
                                using (MultiPartPostBuildWriter post = new MultiPartPostBuildWriter(stream, BOUNDARY, ENCODE))
                                {
                                    for (int i = 0; i < keyset.GetLength(0); i++)
                                    {
                                        post.AddFormText(keyset[i, 0], keyset[i, 1]);
                                    }
                                    post.AddEndBoundary();
                                    d = stream.ToArray();
                                }
                            }

                            //connect
                            {
                                req = (HttpWebRequest)WebRequest.Create(SITE + @"/add_diary.pl");
                                req.Method = "POST";
                                req.ContentType = "multipart/form-data; boundary=" + BOUNDARY;
                                req.ContentLength = d.Length;
                                req.CookieContainer = cookie;

                                using (Stream stream = req.GetRequestStream())
                                {
                                    stream.Write(d, 0, d.Length);
                                }
                            }
                        }
                    }

                    //down
                    {
                        string r = GetResponse(req, ENCODE);
#if DEBUG
#if true
                        TxtDump("add_diary_01(confirm).html", r, ENCODE);
#endif
#endif
                        if (r.IndexOf(ResultStrings.ADD_DIARY_CONFIRM) < 0)
                        {
                            throw new ConfirmDialyUploadFaildException();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="discript"></param>
        /// <param name="datas"></param>
        string AddDialyMain(string title, string discript, byte[][] datas)
        {
            //<form id="diary_form" name="diary" method="post" enctype="multipart/form-data" action="add_diary.pl"> 
            //<input gtbfieldid="4" value="" name="diary_title" class="editareaWidth" size="40">
            //<textarea name="diary_body" cols="65" rows="20" id="diaryBody"></textarea>
            //<p>1枚目<input size="35" name="photo1" type="file"></p>
            //<p>2枚目<input size="35" name="photo2" type="file"></p>
            //<p>3枚目<input size="35" name="photo3" type="file"></p>

            //add_diary.pl
            {
                HttpWebRequest req;

                //up
                {
                    byte[] d;

                    //create data
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            string[,] keyset =
                            {
                                {"diary_title", title},
                                {"diary_body", discript},
                                //{"diary_level_type", "diaryLevelStandard"},
                                //{"id", "" + uid},
                                //{"news_id", ""},
                                //{"campaign_id", ""},
                                //{"invite_campaign", ""},
                                //{"news_title", ""},
                                //{"news_url", ""},
                                //{"movie_id", ""},
                                //{"movie_title", ""},
                                //{"movie_url", ""},
                                {"submit", "main"},
                            };
                            using (MultiPartPostBuildWriter post = new MultiPartPostBuildWriter(stream, BOUNDARY, ENCODE))
                            {

                                for (int i = 0; i < keyset.GetLength(0); i++)
                                {
                                    post.AddFormText(keyset[i, 0], keyset[i, 1]);
                                }

                                if (datas != null)
                                {
                                    for (int i = 0; i < datas.Length; i++)
                                    {
                                        if (datas[i] != null)
                                        {
                                            post.AddFormJpeg("photo" + (i + 1), "photo_" + (i + 1) + ".jpg", datas[i]);
                                        }
                                    }
                                }
                                post.AddEndBoundary();
                            }

                            d = stream.ToArray();
                        }
                    }

                    //connect
                    {
                        req = (HttpWebRequest)WebRequest.Create(SITE + @"/add_diary.pl?id=" + uid);
                        req.Method = "POST";
                        req.ContentType = "multipart/form-data; boundary=" + BOUNDARY;
                        req.ContentLength = d.Length;
                        req.CookieContainer = cookie;

                        using (Stream stream = req.GetRequestStream())
                        {
                            stream.Write(d, 0, d.Length);
                        }
                    }

#if DEBUG
#if true
                    BinDump("form.txt", d);
#endif
#endif

                }

                //down
                {
                    string r = GetResponse(req, ENCODE);
#if DEBUG
#if true
                    TxtDump("add_diary_01(main).html", r, ENCODE);
#endif
#endif
                    if (r.IndexOf(ResultStrings.ADD_DIARY_MAIN) < 0)
                    {
                        throw new MainDialyUploadFaildException();
                    }
                    return r;
                }
            }

        }

        /// <summary>
        /// uidを更新
        /// </summary>
        void UpdateUid()
        {
            uid = GetUid();
        }

        /// <summary>
        /// uidを取得
        /// </summary>
        /// <returns></returns>
        int GetUid()
        {
            string res = GetHtml("/home.pl");

            using (StringReader reader = new StringReader(res))
            {
                String line = null;

                while ((line = reader.ReadLine()) != null)
                {
                    string site;
                    //create regex url
                    {
                        StringBuilder s = new StringBuilder();
                        s.Append(SITE);
                        s.Append(@"show_profile.pl?id=");
                        s.Replace(".", @"\.");
                        s.Replace("?", @"\?");
                        site = s.ToString();
                    }

                    //regex
                    {
                        Regex regex = new Regex("(" + site + ")" + "([0-9]+)" + @"(\S)");
                        Match match = regex.Match(line);

                        if (match.Success)
                        {
                            return int.Parse(match.Groups[2].Value);
                            //Console.WriteLine(match.ToString());
                        }
                    }
                }
            }
            throw new UidNotFoundException();
        }

        /// <summary>
        /// 
        /// </summary>
        void Login()
        {
            //login.pl
            {
                HttpWebRequest req;

                //up
                {
                    byte[] d;

                    //create data
                    {
                        string[,] s =
                        {
                            {"email", mail},
                            {"password", passwd},
                            {"next_url", "/home.pl"},
                        };
                                string t = "";
                        for (int i = 0; i < s.GetLength(0); i++)
                        {
                            t += s[i, 0];
                            t += "=";
                            t += s[i, 1];
                            t += "&";
                        }
                        d = Encoding.ASCII.GetBytes(t);
                    }

                    //connect
                    {
                        req = (HttpWebRequest)WebRequest.Create(SITE + "login.pl");
                        req.Method = "POST";
                        req.ContentType = "application/x-www-form-urlencoded";
                        req.ContentLength = d.Length;
                        req.CookieContainer = cookie;

                        using (Stream stream = req.GetRequestStream())
                        {
                            stream.Write(d, 0, d.Length);
                        }

                    }
                }

                //down
                {
                    string r = GetResponse(req, ENCODE);
#if DEBUG
                    TxtDump("login.html", r, ENCODE);
#endif
                    if (r.IndexOf(ResultStrings.LOGIN) < 0)
                    {
                        throw new LoginFaildException();
                    }
                }
            }
        }

        /// <summary>
        /// 通信用の初期化
        /// </summary>
        public void Init()
        {
            Login();
            UpdateUid();
        }
    }
}
